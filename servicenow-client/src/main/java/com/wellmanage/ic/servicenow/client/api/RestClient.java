package com.wellmanage.ic.servicenow.client.api;

import java.util.Map;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

public interface RestClient {

	public ResponseEntity<String> accessEndpoint(String url, HttpMethod httpMethod);
	
	public ResponseEntity<String> accessEndpoint(String url, HttpMethod httpMethod, Map<String, String> variables);
	
	public ResponseEntity<String> accessEndpoint(String url, HttpMethod httpMethod, String body);
	
	public ResponseEntity<String> accessEndpoint(String url, HttpMethod httpMethod, Map<String, String> variables, String body);
	
	public void addHeader(String headerName, String headerValue);
	
	public void setHeader(String headerName, String headerValue);
}
