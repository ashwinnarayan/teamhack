package com.wellmanage.ic.servicenow.client.api;

import org.springframework.http.ResponseEntity;

public class ServiceNowDataResponse {

	private ResponseEntity<String> responseEntity;

	public ResponseEntity<String> getResponseEntity() {
		return responseEntity;
	}

	public void setResponseEntity(ResponseEntity<String> responseEntity) {
		this.responseEntity = responseEntity;
	}	
	
}
