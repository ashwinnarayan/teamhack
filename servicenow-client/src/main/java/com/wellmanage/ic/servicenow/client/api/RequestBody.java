package com.wellmanage.ic.servicenow.client.api;

import org.springframework.http.MediaType;

public class RequestBody {

	private String content;
	private MediaType contentFormat;

	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public MediaType getContentFormat() {
		return contentFormat;
	}
	
	public void setContentFormat(MediaType contentFormat) {
		this.contentFormat = contentFormat;
	}

}
