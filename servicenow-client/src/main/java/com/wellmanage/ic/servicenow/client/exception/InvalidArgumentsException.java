package com.wellmanage.ic.servicenow.client.exception;

public class InvalidArgumentsException extends Exception {
	
	private static final long serialVersionUID = 3156434024491897466L;

	private String details;
	
	public InvalidArgumentsException() {
		super();
	}
	
	public InvalidArgumentsException(String message) {
		super(message);
	}
	
	public InvalidArgumentsException(String message, Throwable t) {
		super(message, t);
	}

	public InvalidArgumentsException(String message, Throwable t, String details) {
		super(message, t);
		this.details = details;
	}
	
	
	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
}
