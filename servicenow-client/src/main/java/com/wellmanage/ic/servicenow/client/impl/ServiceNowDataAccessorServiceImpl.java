package com.wellmanage.ic.servicenow.client.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;
import com.wellmanage.ic.servicenow.client.api.RestClient;
import com.wellmanage.ic.servicenow.client.api.ServiceNowDataAccessorService;
import com.wellmanage.ic.servicenow.client.enums.ServiceType;
import com.wellmanage.ic.servicenow.client.exception.BadRequestException;
import com.wellmanage.ic.servicenow.client.exception.InvalidArgumentsException;

@Component
public class ServiceNowDataAccessorServiceImpl implements ServiceNowDataAccessorService{

	private static final String EMPTY_JSON = "[]";

	Logger log = LoggerFactory.getLogger(ServiceNowDataAccessorServiceImpl.class);
	
	@Inject
	RestClient restClient;

	@Value("${service-now.base.url}")
	String serviceNowBaseUrl;
	
	@Override
	public ResponseEntity<String> getIncident(String incidentId) throws BadRequestException, InvalidArgumentsException {

		ServiceType svcType = ServiceType.GET_INCIDENT;
		//Build URL
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
											.fromHttpUrl(serviceNowBaseUrl)
											.pathSegment(svcType.getPath())
											.queryParam("sysparm_query","number="+incidentId)
											//.queryParam("sysparm_fields","number,short_description,assigned_to,state,urgency,closed_by,catgeory,u_incident_manager,impact,priority,description,u_environment")
											.queryParam("sysparm_exclude_reference_link",true)
											.queryParam("sysparm_display_value",true);
		
		UriComponents uri = uriBuilder.build();
		String url = uri.toUriString();
		
		log.debug("Get Incident URL::" + url);
		
		//Get the Http Method
		HttpMethod httpMethod = svcType.getHttpMethod();

		//Process Request
		ResponseEntity<String> responseEntity = restClient.accessEndpoint(url,httpMethod);	

		return responseEntity;
		
	
	}

	@Override
	public ResponseEntity<String> getIncidentTasks(String incidentId)
			throws BadRequestException, InvalidArgumentsException {
		ServiceType svcType = ServiceType.GET_INCIDENT_TASKS;
		//Build URL
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
											.fromHttpUrl(serviceNowBaseUrl)
											.pathSegment(svcType.getPath())
											.queryParam("parent="+incidentId)
											.queryParam("sysparm_exclude_reference_link",true)
											.queryParam("sysparm_display_value",true);
		
		UriComponents uri = uriBuilder.build();
		String url = uri.toUriString();
		
		log.debug("Get Incident Tasks URL::" + url);
		
		//Get the Http Method
		HttpMethod httpMethod = svcType.getHttpMethod();

		//Process Request
		ResponseEntity<String> responseEntity = restClient.accessEndpoint(url,httpMethod);	

		return responseEntity;
	}

	@Override
	public ResponseEntity<String> createIncident(Map<String, String> incidentEntry) 
			throws BadRequestException, InvalidArgumentsException{
		ServiceType svcType = ServiceType.CREATE_INCIDENT;
		//Build URL
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
											.fromHttpUrl(serviceNowBaseUrl)
											.pathSegment(svcType.getPath());
		
		UriComponents uri = uriBuilder.build();
		String url = uri.toUriString();
		
		log.debug("Create Incident Tasks URL::" + url);
		
		Gson gson = new Gson();
		String reqBody = gson.toJson(incidentEntry);
		
		log.debug("Request Body in JSON::" + reqBody);
		
		//Get the Http Method
		HttpMethod httpMethod = svcType.getHttpMethod();

		//Process Request
		ResponseEntity<String> responseEntity = restClient.accessEndpoint(url,httpMethod, reqBody);	

		return responseEntity;
	}

	@Override
	public ResponseEntity<String> updateIncident(String incidentId, Map<String, String> incidentEntry)
			throws BadRequestException, InvalidArgumentsException{
		//Get Incident first to get Sys Id. Saving some trouble for consumer and letting them drive through incident id
		ResponseEntity<String> incidentDetResponse = getIncident(incidentId);
		Map responseMap = new HashMap<String, Object>();
		Gson gson = new Gson();
		responseMap = gson.fromJson(incidentDetResponse.getBody(), responseMap.getClass());
		
		List<Map<String, Object>> resultList = (ArrayList<Map<String, Object>>)responseMap.get("result");
		
		String ticketSysId = (String)resultList.get(0).get("sys_id");
		
		ServiceType svcType = ServiceType.UPDATE_INCIDENT;
		//Build URL
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
											.fromHttpUrl(serviceNowBaseUrl)
											.pathSegment(svcType.getPath()+"/"+ticketSysId);
		
		UriComponents uri = uriBuilder.build();
		String url = uri.toUriString();
		
		log.debug("Update Incident Tasks URL::" + url);
		
		String reqBody = gson.toJson(incidentEntry);
		
		log.debug("Request Body in JSON::" + reqBody);
		
		//Get the Http Method
		HttpMethod httpMethod = svcType.getHttpMethod();

		//Process Request
		ResponseEntity<String> responseEntity = restClient.accessEndpoint(url,httpMethod, reqBody);	

		return responseEntity;
	}

	@Override
	public ResponseEntity<String> getSysUser(String userId) throws BadRequestException, InvalidArgumentsException {
		ServiceType svcType = ServiceType.GET_USER;
		//Build URL
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
											.fromHttpUrl(serviceNowBaseUrl)
											.pathSegment(svcType.getPath())
											.queryParam("user_name="+userId)
											.queryParam("sysparm_display_value",true);
		
		UriComponents uri = uriBuilder.build();
		String url = uri.toUriString();
		
		log.debug("Get User URL::" + url);
		
		//Get the Http Method
		HttpMethod httpMethod = svcType.getHttpMethod();

		//Process Request
		ResponseEntity<String> responseEntity = restClient.accessEndpoint(url,httpMethod);	

		return responseEntity;
	}

	@Override
	public ResponseEntity<String> findUser(String findByName) throws BadRequestException, InvalidArgumentsException {
		ServiceType svcType = ServiceType.GET_USER;
		//Build URL
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
											.fromHttpUrl(serviceNowBaseUrl)
											.pathSegment(svcType.getPath())
											.queryParam("sysparm_query=last_nameLIKE"+findByName+"^ORfirst_nameLIKE"+findByName)
											.queryParam("sysparm_display_value",true);
		
		UriComponents uri = uriBuilder.build();
		String url = uri.toUriString();
		
		log.debug("Get User URL::" + url);
		
		//Get the Http Method
		HttpMethod httpMethod = svcType.getHttpMethod();

		//Process Request
		ResponseEntity<String> responseEntity = restClient.accessEndpoint(url,httpMethod);	

		return responseEntity;
	}

	@Override
	public ResponseEntity<String> getCriticalIncidents() throws BadRequestException, InvalidArgumentsException {
		ServiceType svcType = ServiceType.GET_INCIDENT;
		//Build URL
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
											.fromHttpUrl(serviceNowBaseUrl)
											.pathSegment(svcType.getPath())
											.queryParam("sysparm_query=priorityIN1,2")
											.queryParam("sysparm_limit","10")
											.queryParam("sysparm_exclude_reference_link",true)
											.queryParam("sysparm_display_value",true);
		
		UriComponents uri = uriBuilder.build();
		String url = uri.toUriString();
		
		log.debug("Get Incident URL::" + url);
		
		//Get the Http Method
		HttpMethod httpMethod = svcType.getHttpMethod();

		//Process Request
		ResponseEntity<String> responseEntity = restClient.accessEndpoint(url,httpMethod);	

		return responseEntity;
	}
	
}
