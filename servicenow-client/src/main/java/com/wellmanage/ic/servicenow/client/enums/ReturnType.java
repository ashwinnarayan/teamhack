package com.wellmanage.ic.servicenow.client.enums;

public enum ReturnType {

	ARRAY, OBJECT;
}
