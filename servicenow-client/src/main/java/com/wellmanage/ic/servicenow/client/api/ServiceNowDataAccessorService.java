package com.wellmanage.ic.servicenow.client.api;

import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.wellmanage.ic.servicenow.client.exception.BadRequestException;
import com.wellmanage.ic.servicenow.client.exception.InvalidArgumentsException;

public interface ServiceNowDataAccessorService {

	public ResponseEntity<String> getIncident(String incidentId) throws BadRequestException, InvalidArgumentsException;

	public ResponseEntity<String> getIncidentTasks(String incidentId) throws BadRequestException, InvalidArgumentsException;
	
	public ResponseEntity<String> createIncident(Map<String, String> incidentEntry) throws BadRequestException, InvalidArgumentsException;
	
	public ResponseEntity<String> updateIncident(String incidentId, Map<String, String> incidentEntry) throws BadRequestException, InvalidArgumentsException;
	
	public ResponseEntity<String> getSysUser(String userId) throws BadRequestException, InvalidArgumentsException;
	
	public ResponseEntity<String> findUser(String findByName) throws BadRequestException, InvalidArgumentsException;
	
	public ResponseEntity<String> getCriticalIncidents() throws BadRequestException, InvalidArgumentsException;
	
	//TODO
	//1. Create Incident - COMPLETED
	//2. Create Tasks 
	//3. Get ITLT Users 
	//4. Get Incident Tasks - COMPLETED
	//5. Get Incident - COMPLETED
	//6. Update Incident
	//7. Update Tasks 
	//8. Get User by NT id


}
