package com.wellmanage.ic.servicenow.client.enums;

import org.springframework.http.HttpMethod;

public enum ServiceType {

	GET_USER("api/now/table/sys_user", HttpMethod.GET, ReturnType.OBJECT),
	GET_INCIDENT("api/now/table/incident", HttpMethod.GET, ReturnType.OBJECT),
	GET_INCIDENT_TASKS("api/now/table/task", HttpMethod.GET, ReturnType.OBJECT),
	CREATE_INCIDENT("api/now/table/incident", HttpMethod.POST, ReturnType.OBJECT),
	UPDATE_INCIDENT("api/now/table/incident", HttpMethod.PUT, ReturnType.OBJECT);

	String path;
	HttpMethod httpMethod;
	ReturnType returnType;
	
	private ServiceType(String path, HttpMethod httpMethod, ReturnType returnType) {
		this.path = path;
		this.httpMethod = httpMethod;
		this.returnType = returnType;
	}
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}

	public HttpMethod getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(HttpMethod httpMethod) {
		this.httpMethod = httpMethod;
	}

	public ReturnType getReturnType() {
		return returnType;
	}

	public void setReturnType(ReturnType returnType) {
		this.returnType = returnType;
	}
	
}
