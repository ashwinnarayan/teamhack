package com.wellmanage.ic.servicenow.client.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.wellmanage.ic.servicenow.client.ServiceNowClientServiceConfiguration;

@Retention(value=java.lang.annotation.RetentionPolicy.RUNTIME)
@Target(value={java.lang.annotation.ElementType.TYPE})
@Import(ServiceNowClientServiceConfiguration.class)
public @interface EnableServiceNowClientService {

}
