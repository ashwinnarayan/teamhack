package com.wellmanage.ic.servicenow.client.exception;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;

@Component
public class ServiceNowClientResponseErrorHandler implements ResponseErrorHandler{

	private ResponseErrorHandler defErrorHandler = new DefaultResponseErrorHandler();
	
	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		return defErrorHandler.hasError(response);
	}

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		String body = IOUtils.toString(response.getBody());
		new BadRequestException(response.getStatusCode(), body);
	}

}
