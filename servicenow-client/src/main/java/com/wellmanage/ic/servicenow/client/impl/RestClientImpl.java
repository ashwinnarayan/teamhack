package com.wellmanage.ic.servicenow.client.impl;

import java.util.Base64;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import com.wellmanage.ic.servicenow.client.api.RestClient;


@Component
public class RestClientImpl implements RestClient {
	
	private Logger log = LoggerFactory.getLogger(RestClientImpl.class);

	@Value("${auth.username}")
	private String username;

	@Value("${auth.password}")
	private String password;
	
	@Inject
	private ResponseErrorHandler errorHandler;
	
	private HttpHeaders httpHeaders;

	@PostConstruct
	public void init() {
		createHttpHeaders();
	}
	
	private void createHttpHeaders(){
		httpHeaders = new HttpHeaders(){
			private static final long serialVersionUID = 2608141409819077237L;
			{
				 String auth = username + ":" + password;
		         byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes());
		         String authHeader = "Basic " + new String( encodedAuth );
		         set( "Authorization", authHeader );
		         set ("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		         set ("Accept", MediaType.APPLICATION_JSON_VALUE);
		      }
		   };
	}

	public void setHeader(String headerName, String headerValue) {
		httpHeaders.set(headerName, headerValue);
	}

	public void addHeader(String headerName, String headerValue) {
		httpHeaders.add(headerName, headerValue);
	}
	
	public ResponseEntity<String> accessEndpoint(String url, HttpMethod httpMethod, Map<String, String> variables) {
		return accessEndpoint(url, httpMethod, variables, null);
	}
	
	public ResponseEntity<String> accessEndpoint(String url, HttpMethod httpMethod) {
		return accessEndpoint(url, httpMethod, null, null);
	}	

	public ResponseEntity<String> accessEndpoint(String url, HttpMethod httpMethod, String body) {
		return accessEndpoint(url, httpMethod, null, body);		
	}	
	
	public ResponseEntity<String> accessEndpoint(String url, HttpMethod httpMethod, Map<String, String> variables, String body) {
		
		ResponseEntity<String> respEntity = null;
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(errorHandler);
		HttpEntity<String> httpEntity = null;
		
/*		String sId = (null != httpHeaders.get("JSESSIONID") ? httpHeaders.get("JSESSIONID").get(0) : null);
		log.debug("jsesion id in http header::: " + sId);
		
		if (null != sId) {
			httpHeaders.set("JSESSIONID", sId);	
		}
*/		
		
		if (!StringUtils.isEmpty(body)) {
			httpEntity = new HttpEntity<String>(body, httpHeaders);
		} else {
			httpEntity = new HttpEntity<String>(httpHeaders);
		}
		
		if (null == variables || variables.size() == 0) {
			respEntity = restTemplate.exchange(url, httpMethod, httpEntity, String.class);
		} else {
			respEntity = restTemplate.exchange(url, httpMethod, httpEntity, String.class, variables);
		}
		
		HttpHeaders headers = respEntity.getHeaders();

		List<String> cookiesList = headers.get("Set-Cookie");
		
		for (String cookie : cookiesList)
		{
			log.debug("My Cookies::::"+ cookie);
		}

/*		log.debug("Cookie value set :"+ headers.get("Set-Cookie").stream().collect(Collectors.joining(";")));
		setHeader("Set-Cookie", headers.get("Set-Cookie").stream().collect(Collectors.joining(";")));
*/		
		
/*		Set<Entry<String, List<String>>> entrySet = headers.entrySet();
		
		for (Iterator<Entry<String, List<String>>> iter=entrySet.iterator();iter.hasNext();) {
			Entry<String, List<String>> entry = iter.next();
			log.debug("Keyyyyyyyyyyy "+ entry.getKey());
			log.debug("Valueeeeeeeee "+ entry.getValue());
			
		}
*/		
		
		return respEntity;
	}	
	
}
