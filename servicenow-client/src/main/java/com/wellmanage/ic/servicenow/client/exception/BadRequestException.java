package com.wellmanage.ic.servicenow.client.exception;

import org.springframework.http.HttpStatus;

public class BadRequestException extends Exception{

	private static final long serialVersionUID = -2336876789879631793L;

	private HttpStatus statusCode;

    private String body;

    public BadRequestException(String msg) {
        super(msg);
    }
    
    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
    }    

    public BadRequestException(HttpStatus statusCode, String body) {
        super(body);
        this.statusCode = statusCode;
        this.body = body;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(HttpStatus statusCode) {
        this.statusCode = statusCode;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
	
}
