package com.wellmanage.ic.servicenow.client;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Bala
 * 
 * Core Configuration class to load all the components needed for Reference Data client API
 *
 */
@Configuration
@EnableConfigurationProperties
@ComponentScan(basePackageClasses = {ServiceNowClientServiceConfiguration.class})
public class ServiceNowClientServiceConfiguration{

}
