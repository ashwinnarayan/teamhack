package com.wellmanage.ic.servicenow.client.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.wellmanage.ic.servicenow.client.annotations.EnableServiceNowClientService;

@Configuration
@PropertySource("classpath:application.properties")
@ComponentScan(basePackageClasses = {AppConfig.class})
@EnableServiceNowClientService
public class AppConfig {

	//To resolve ${} in @Value
	@Bean
	public PropertySourcesPlaceholderConfigurer applicationProperties() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
}
