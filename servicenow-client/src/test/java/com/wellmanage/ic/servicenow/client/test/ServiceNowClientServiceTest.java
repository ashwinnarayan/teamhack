package com.wellmanage.ic.servicenow.client.test;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.wellmanage.ic.servicenow.client.api.ServiceNowDataAccessorService;
import com.wellmanage.ic.servicenow.client.impl.ServiceNowDataAccessorServiceImpl;

@Component
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader=AnnotationConfigContextLoader.class, classes={AppConfig.class})
public class ServiceNowClientServiceTest {

	private static final String TEST_INCIDENT_ID = "INC1192711";
	private final Logger log = LoggerFactory.getLogger(ServiceNowDataAccessorServiceImpl.class);
	private static final int STATUS_200 = 200;
	private static final int STATUS_201 = 201;

	@Rule public TestName name = new TestName();
	
	@Inject
	private ServiceNowDataAccessorService serviceNowDataAccessorService;	

	private ResponseEntity<String> response = null;
	

	@Test
	public void testGetIncidentById() throws Exception{
		response = serviceNowDataAccessorService.getIncident(TEST_INCIDENT_ID);
		
		assertTrue(response.getStatusCode().value() == STATUS_200);
	}	
	
	@Test
	public void testGetIncidentTasks() throws Exception{
		response = serviceNowDataAccessorService.getIncidentTasks(TEST_INCIDENT_ID);
		
		assertTrue(response.getStatusCode().value() == STATUS_200);
	}	

	@Test
	public void testCreateIncident() throws Exception{
		Map<String, String> incidentMap = new HashMap<String, String>();
		incidentMap.put("short_description","OMG. This is an another test ticket in service now....");
		
		response = serviceNowDataAccessorService.createIncident(incidentMap);
		assertTrue(response.getStatusCode().value() == STATUS_201);
	}		

	@Test
	public void testUpdateIncident() throws Exception{
		Map<String, String> incidentMap = new HashMap<String, String>();
		incidentMap.put("short_description","OMG. This is an another test ticket in service now....AFTER UPDATE");
		
		response = serviceNowDataAccessorService.updateIncident("INC1192717", incidentMap);
		assertTrue(response.getStatusCode().value() == STATUS_200);
	}		

	@Test
	public void testGetUser() throws Exception{
		response = serviceNowDataAccessorService.getSysUser("krishb");
		assertTrue(response.getStatusCode().value() == STATUS_200);
	}		
	
	
	@After
	public void displayResponse() {
		log.debug("============================Test Results for " + name.getMethodName() + " ================================" );
		log.debug("Response Status Code: "+ response.getStatusCode().value());
		log.debug("Response Body: " + response.getBody());
		log.debug("**********************************************************************************************************\n");
		
	}

}


