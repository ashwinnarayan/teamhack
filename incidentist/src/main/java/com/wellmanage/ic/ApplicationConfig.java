package com.wellmanage.ic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

import com.wellmanage.ic.servicenow.client.annotations.EnableServiceNowClientService;

/**
 * The Class ApplicationConfig.
 */
@Configuration
@PropertySource("classpath:application.properties")
@EnableServiceNowClientService
public class ApplicationConfig {

	/** The Constant APPLICATION_VERSION_KEY. */
	private static final String APPLICATION_VERSION_KEY = "app.version";

	/** The Constant APPLICATION_PORT_KEY. */
	private static final String APPLICATION_PORT_KEY = "application.tomcat.website.port";

	/** The Constant APPLICATION_PROXY_KEY. */
	private static final String APPLICATION_PROXY_KEY = "application.tomcat.website.proxy";

	/** The constant EMAIL_HOST_KEY. */
	private static final String EMAIL_HOST_KEY = "wmc.emailHost";

	/** The constant EMAIL_PORT_KEY. */
	private static final String EMAIL_PORT_KEY = "wmc.emailPort";

	/** The Constant APPLICATION_CONTEXT. */
	private static final String APPLICATION_CONTEXT = "server.contextPath";

	/** The Constant UNDEFINED. */
	private static final String UNDEFINED = "n/a";

	/** URL prefix */
	private static final String URL_PREFIX = "http://";

	/** URL prefix */
	private static final String URL_POSTFIX = "/";

	/** The environment. */
	private Environment environment;

	/**
	 * Gets the application version.
	 * 
	 * @return the application version
	 */
	public String getApplicationVersion() {

		return environment.getProperty(APPLICATION_VERSION_KEY, String.class,
				UNDEFINED);
	}

	/**
	 * Gets the application proxy.
	 * 
	 * @return the application proxy
	 */
	public String getApplicationProxy() {

		return environment.getProperty(APPLICATION_PROXY_KEY, String.class);
	}

	/**
	 * Gets the application context.
	 * 
	 * @return the application context
	 */
	public String getApplicationContext() {

		return environment.getProperty(APPLICATION_CONTEXT, String.class);
	}

	/**
	 * Gets the application port.
	 * 
	 * @return the application port
	 */
	public String getApplicationPort() {

		return environment.getProperty(APPLICATION_PORT_KEY, String.class);
	}

	/**
	 * Gets base URL of website.
	 * 
	 * @return the base URL
	 */
	public String getBaseURL() {

		return URL_PREFIX + getApplicationProxy() + getApplicationContext()
				+ URL_POSTFIX;
	}

	/**
	 * Gets the email host
	 * 
	 * @return the email host
	 */
	public String getEmailHost() {

		return environment.getProperty(EMAIL_HOST_KEY, String.class);
	}

	/**
	 * Gets email port.
	 * 
	 * @return the email port
	 */
	public Integer getEMailPort() {

		return environment.getProperty(EMAIL_PORT_KEY, Integer.class);
	}

	/**
	 * Sets the environment.
	 * 
	 * @param environment
	 *            the new environment
	 */
	@Autowired
	public void setEnvironment(Environment environment) {

		this.environment = environment;
	}

	/**
	 * Get active profiles.
	 * 
	 * @return string [ ]
	 */
	public String[] getActiveProfiles() {
		return environment.getActiveProfiles();
	}

	//To resolve ${} in @Value
	@Bean
	public PropertySourcesPlaceholderConfigurer applicationProperties() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
}