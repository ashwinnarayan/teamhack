package com.wellmanage.ic.database;

import java.io.Serializable;
import java.util.Date;

public class CommunicationBean implements Serializable{

	private String commType;//ITLT or Business
	private String text;
	private Date commDate;
	
	public String getCommType() {
		return commType;
	}
	public void setCommType(String commType) {
		this.commType = commType;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCommDate() {
		return commDate;
	}
	public void setCommDate(Date commDate) {
		this.commDate = commDate;
	}
	
}
