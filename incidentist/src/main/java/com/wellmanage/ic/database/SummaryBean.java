package com.wellmanage.ic.database;

import java.io.Serializable;
import java.util.Date;

public class SummaryBean implements Serializable{

	private String userName;
	private Date insertDate;
	private String summaryText;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}
	public String getSummaryText() {
		return summaryText;
	}
	public void setSummaryText(String summaryText) {
		this.summaryText = summaryText;
	}
	
	
	
}
