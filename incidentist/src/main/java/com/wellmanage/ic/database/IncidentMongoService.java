package com.wellmanage.ic.database;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IncidentMongoService {

	@Autowired
	private IncidentRepository incidentRepository;
	
	
	public IncidentBean addIncident(String incidentNumber) {
		IncidentBean incidentBean = new IncidentBean();
		incidentBean.setIncidentId(incidentNumber);
		
		return incidentRepository.save(incidentBean);
	}

	
	public IncidentBean getIncident(String incidentNumber) {
		return incidentRepository.findOne(incidentNumber);
	}

	public IncidentBean addDiscussion(DiscussionBean discussion, String incidentNumber) {
		IncidentBean incidentBean = incidentRepository.findOne(incidentNumber);
		if (null != incidentBean) {
			if (null != incidentBean.getDiscussions()) {
				incidentBean.getDiscussions().add(discussion);
				
			} else {
				Collection<DiscussionBean> collBeanList = new ArrayList<DiscussionBean>();
				collBeanList.add(discussion);
				incidentBean.setDiscussions(collBeanList);
			}
		}
		
		return incidentRepository.save(incidentBean);
	}

	public IncidentBean addSummary(SummaryBean summary, String incidentNumber) {
		IncidentBean incidentBean = incidentRepository.findOne(incidentNumber);
		if (null != incidentBean) {
			if (null != incidentBean.getSummaries()) {
				incidentBean.getSummaries().add(summary);
				
			} else {
				Collection<SummaryBean> sumList = new ArrayList<SummaryBean>();
				sumList.add(summary);
				incidentBean.setSummaries(sumList);
			}
		}
		
		return incidentRepository.save(incidentBean);
	}
	
	public IncidentBean addCommunication(CommunicationBean communication, String incidentNumber) {
		IncidentBean incidentBean = incidentRepository.findOne(incidentNumber);
		if (null != incidentBean) {
			if (null != incidentBean.getCommunications()) {
				incidentBean.getCommunications().add(communication);
				
			} else {
				Collection<CommunicationBean> sumList = new ArrayList<CommunicationBean>();
				sumList.add(communication);
				incidentBean.setCommunications(sumList);
			}
		}
		
		return incidentRepository.save(incidentBean);
	}

	
}
