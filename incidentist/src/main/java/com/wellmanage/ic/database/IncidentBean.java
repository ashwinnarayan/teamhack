package com.wellmanage.ic.database;

import java.io.Serializable;
import java.util.Collection;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "incidents")
public class IncidentBean implements Serializable{

	@Id
	private String incidentId;
	private Collection<SummaryBean> summaries;
	private Collection<DiscussionBean> discussions;
	private Collection<CommunicationBean> communications;
	
	public String getIncidentId() {
		return incidentId;
	}
	public void setIncidentId(String incidentId) {
		this.incidentId = incidentId;
	}
	public Collection<SummaryBean> getSummaries() {
		return summaries;
	}
	public void setSummaries(Collection<SummaryBean> summaries) {
		this.summaries = summaries;
	}
	public Collection<DiscussionBean> getDiscussions() {
		return discussions;
	}
	public void setDiscussions(Collection<DiscussionBean> discussions) {
		this.discussions = discussions;
	}
	public Collection<CommunicationBean> getCommunications() {
		return communications;
	}
	public void setCommunications(Collection<CommunicationBean> communications) {
		this.communications = communications;
	}
	 
	
	
}
