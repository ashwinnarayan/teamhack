package com.wellmanage.ic.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wellmanage.ic.domain.Task;
import com.wellmanage.ic.repository.TaskRepository;

@RestController
@RequestMapping("/api/tasks")
public class TaskController {

	@Autowired
	private TaskRepository repo;

	@RequestMapping(method = RequestMethod.GET)
	public List<Task> findItems() {
		return repo.findAll();
	}

	@RequestMapping(method = RequestMethod.POST)
	public Task addItem(@RequestBody Task item) {
		item.setId(null);
		return repo.saveAndFlush(item);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteItem(@PathVariable Integer id) {
		repo.delete(id);
	}

}
