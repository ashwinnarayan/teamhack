package com.wellmanage.ic.domain;

import com.wellmanage.weos.sso.hantweb.HantwebUser;


public class IncidentUser {

	private HantwebUser hantWebUser;
	private String serviceNowUser;
	
	public String getServiceNowUser() {
		return serviceNowUser;
	}

	public void setServiceNowUser(String serviceNowUser) {
		this.serviceNowUser = serviceNowUser;
	}

	public HantwebUser getHantWebUser() {
		return hantWebUser;
	}

	public void setHantWebUser(HantwebUser hantWebUser) {
		this.hantWebUser = hantWebUser;
	}
	
	
	
}
