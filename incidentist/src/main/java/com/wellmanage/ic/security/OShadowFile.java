package com.wellmanage.ic.security;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Routines for the Wellington Oshadow File. Ripped directly from the Wellington wmc-common routines.
 *
 * @author NarteT
 * @see com.wellmanage.utility.AppUtils
 */
public final class OShadowFile {

    /** The Constant OSHADOW_PROTOCOL. */
    private static final String OSHADOW_URL_PREFIX = "oshadow://";

    /** The Constant AND_DELIMITER. */
    private static final String AND_DELIMITER = "&";

    /** The Constant EQUAL_DELIMITER. */
    private static final String EQUAL_DELIMITER = "=";

    /** The Constant OSHADOW_APP_ID. */
    private static final String OSHADOW_APP_ID_KEY = "appId";

    /** The Constant OSHADOW_APP_ID. */
    private static final String OSHADOW_USER_ID_KEY = "userId";

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(OShadowFile.class);

    /**
     * Instantiates a new oshadow file.
     */
    private OShadowFile() {

    }

    /**
     * Gets the oshadow params.
     *
     * @param oshadowUrl the oshadow url
     * @return the oshadow params
     */
    public static String[] getOshadowParams(String oshadowUrl) {

        if (!oshadowUrl.startsWith(OSHADOW_URL_PREFIX)) {
            LOGGER.error("Incorrect format for oshadow url. Please use the format 'oshadow://appId=<appid>&userId=<username>'");
            return null;
        }
        String[] shadowFileParams = StringUtils.substringAfter(oshadowUrl, OSHADOW_URL_PREFIX).split(AND_DELIMITER);
        String userId = null;
        String appId = null;
        for (String entry : shadowFileParams) {
            userId = entry.contains(OSHADOW_USER_ID_KEY) ? StringUtils.split(entry, EQUAL_DELIMITER)[1].trim() : userId;
            appId = entry.contains(OSHADOW_APP_ID_KEY) ? StringUtils.split(entry, EQUAL_DELIMITER)[1].trim() : appId;
        }
        if (StringUtils.isEmpty(userId)) {
            LOGGER.error("User Id has not been specified in oshadow url.");
            return null;
        }
        if (StringUtils.isEmpty(appId)) {
            LOGGER.error("Application Id has not been specified in oshadow url");
            return null;
        }
        LOGGER.info("returning database password for oshadow id:{} and app:{}", userId, appId);
        return new String[] { userId, appId };
    }

    /**
     * Gets the oshadow pwd by sid.
     *
     * @param lookupSid the lookup sid
     * @param lookupUid the lookup uid
     * @return the oshadow pwd by sid
     */
    @SuppressWarnings("unused")
    public static String getOshadowPwdBySid(String lookupSid, String lookupUid) {

        String uid = null;
        String pwd = null;
        String sid = null;
        String alias = null;
        String line = null;
        try {
            BufferedReader oshadow = new BufferedReader(new FileReader(System.getProperty("user.home")
                + System.getProperty("file.separator") + "oshadow"));
            while ((line = oshadow.readLine()) != null) {
                if (line.startsWith(":")) {
                    line = "*" + line;
                }
                if (line.startsWith(":")) {
                    line = "*" + line;
                }
                try {
                    StringTokenizer st = new StringTokenizer(line, ":");
                    sid = st.nextToken();
                    uid = st.nextToken();
                    pwd = st.nextToken();
                    alias = st.nextToken();
                } catch (NoSuchElementException e) {
                    continue;
                }
                if (lookupSid.equals(sid) && lookupUid.equals(uid)) {
                    break;
                }
                pwd = null;
            }
            oshadow.close();
        } catch (IOException ioe) {
            pwd = null;
        }
        return pwd;
    }

    /**
     * Gets the oshadow pwd by alias.
     *
     * @param lookupAlias the lookup alias
     * @param lookupUid the lookup uid
     * @return the oshadow pwd by alias
     */
    @SuppressWarnings("unused")
    public static String getOshadowPwdByAlias(String lookupAlias, String lookupUid) {

        String uid = null;
        String pwd = null;
        String sid = null;
        String alias = null;
        String line = null;
        try {
            BufferedReader oshadow = new BufferedReader(new FileReader(System.getProperty("user.home")
                + System.getProperty("file.separator") + "oshadow"));
            while ((line = oshadow.readLine()) != null) {
                if (line.startsWith(":")) {
                    line = "*" + line;
                }
                if (line.startsWith(":")) {
                    line = "*" + line;
                }
                try {
                    StringTokenizer st = new StringTokenizer(line, ":");
                    sid = st.nextToken();
                    uid = st.nextToken();
                    pwd = st.nextToken();
                    alias = st.nextToken();
                } catch (NoSuchElementException e) {
                    continue;
                }
                if (lookupAlias.equals(alias) && lookupUid.equals(uid)) {
                    break;
                }
                pwd = null;
            }
            oshadow.close();
        } catch (IOException e) {
            pwd = null;
        }
        return pwd;
    }

    /**
     * The method retrieves all username-password information from oshadow file for given alias.
     *
     * @param lookupAlias the lookup alias
     * @return the map (username-password) for given alias
     */
    @SuppressWarnings("unused")
    public static Map<String, String> getAllOshadowUsrPwdByAlias(String lookupAlias) {

        Map<String, String> usernamePassword = new HashMap<String, String>();
        String uid = null;
        String pwd = null;
        String sid = null;
        String alias = null;
        String line = null;
        try {
            BufferedReader oshadow = new BufferedReader(new FileReader(System.getProperty("user.home")
                + System.getProperty("file.separator") + "oshadow"));
            while ((line = oshadow.readLine()) != null) {
                if (line.startsWith(":")) {
                    line = "*" + line;
                }
                if (line.startsWith(":")) {
                    line = "*" + line;
                }
                try {
                    StringTokenizer st = new StringTokenizer(line, ":");
                    sid = st.nextToken();
                    uid = st.nextToken();
                    pwd = st.nextToken();
                    alias = st.nextToken();
                } catch (NoSuchElementException e) {
                    continue;
                }
                if (lookupAlias.equals(alias)) {
                    usernamePassword.put(uid, pwd);
                }
            }
            oshadow.close();
        } catch (IOException e) {
            LOGGER.error("Unable to read oshadow file", e);
        }
        return usernamePassword;
    }

    /**
     * Gets the oshadow alias by sid.
     *
     * @param lookupSid the lookup sid
     * @return the oshadow alias by sid
     */
    @SuppressWarnings("unused")
    public static String getOshadowAliasBySid(String lookupSid) {

        String uid = null;
        String pwd = null;
        String sid = null;
        String alias = null;
        String line = null;
        try {
            BufferedReader oshadow = new BufferedReader(new FileReader(System.getProperty("user.home") + "/oshadow"));
            while ((line = oshadow.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(line, ":");
                sid = st.nextToken();
                uid = st.nextToken();
                pwd = st.nextToken();
                alias = st.nextToken();
                if (lookupSid.equals(sid)) {
                    break;
                }
                alias = null;
            }
            oshadow.close();
        } catch (IOException e) {
            alias = null;
        }
        return alias;
    }

}
