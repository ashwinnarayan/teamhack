package com.wellmanage.ic.security;

/**
 * The Class HantwebPrincipal.
 */
public class HantwebPrincipal {

    /** The timestamp. */
    private String timestamp;

    /** The user login. */
    private String userLogin;

    /**
     * Instantiates a new hantweb principal.
     *
     * @param userLogin the user login
     * @param timestamp the timestamp
     */
    public HantwebPrincipal(String userLogin, String timestamp) {

        super();
        this.userLogin = userLogin;
        this.timestamp = timestamp;
    }

    /**
     * Gets the timestamp.
     *
     * @return the timestamp
     */
    public String getTimestamp() {

        return timestamp;
    }

    /**
     * Gets the user login.
     *
     * @return the user login
     */
    public String getUserLogin() {

        return userLogin;
    }

    /**
     * Sets the timestamp.
     *
     * @param timestamp the new timestamp
     */
    public void setTimestamp(String timestamp) {

        this.timestamp = timestamp;
    }

    /**
     * Sets the user login.
     *
     * @param userLogin the new user login
     */
    public void setUserLogin(String userLogin) {

        this.userLogin = userLogin;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {

        return "HantwebPrincipal: userLogin=" + userLogin + ", timestamp=" + timestamp;
    }
}