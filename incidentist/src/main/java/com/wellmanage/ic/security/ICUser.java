package com.wellmanage.ic.security;

import java.util.ArrayList;
import java.util.Collection;

import com.wellmanage.ic.dto.BaseDto;

public class ICUser extends BaseDto {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The access granted. */
	private boolean accessGranted;

	/** The email address. */
	private final String emailAddress;

	/** The first name. */
	private final String firstName;

	/** The full name. */
	private final String fullName;

	/** The initials. */
	private final String initials;

	/** The last name. */
	private final String lastName;

	/** The login. */
	private final String login;

	private final String ntLogin;

	private final String personTitle;

	/** The person id. */
	private final String personId;

	/** The user privileges. */
	private final Collection<UserPriv> userPrivileges = new ArrayList<UserPriv>();

	private Long groups;

	/**
	 * Instantiates a new deal interest user.
	 * 
	 * @param login
	 *            the login
	 * @param personId
	 *            the person id
	 * @param initials
	 *            the initials
	 * @param emailAddress
	 *            the email address
	 * @param firstName
	 *            the first name
	 * @param lastName
	 *            the last name
	 * @param fullName
	 *            the full name
	 */
	public ICUser(String login, String personId, String initials,
			String emailAddress, String firstName, String lastName,
			String fullName, String ntLogin, String personTitle, Long groups) {

		super();
		this.login = login;
		this.personId = personId;
		this.initials = initials;
		this.emailAddress = emailAddress;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.ntLogin = ntLogin;
		this.personTitle = personTitle;
		this.groups = groups;
	}

	/**
	 * Adds the user privileges.
	 * 
	 * @param userPrivilegesToAdd
	 *            the user privileges to add
	 * @param userAccessReadPriv
	 *            the user access read priv
	 * @param userAccessReadOnlyPriv
	 *            the user access read only priv
	 * @param userAccessEditorPriv
	 *            the user access editor priv
	 * @param userAccessAdminPriv
	 *            the user access admin priv
	 */
	public void addUserPrivileges(Collection<UserPriv> userPrivileges) {
		this.userPrivileges.addAll(userPrivileges);
	}

	/**
	 * Gets the email address.
	 * 
	 * @return the email address
	 */
	public String getEmailAddress() {

		return emailAddress;
	}

	/**
	 * Gets the first name.
	 * 
	 * @return the first name
	 */
	public String getFirstName() {

		return firstName;
	}

	/**
	 * Gets the full name.
	 * 
	 * @return the full name
	 */
	public String getFullName() {

		return fullName;
	}

	/**
	 * Gets the initials.
	 * 
	 * @return the initials
	 */
	public String getInitials() {

		return initials;
	}

	/**
	 * Gets the last name.
	 * 
	 * @return the last name
	 */
	public String getLastName() {

		return lastName;
	}

	/**
	 * Gets the login.
	 * 
	 * @return the login
	 */
	public String getLogin() {

		return login;
	}

	/**
	 * Gets the person id.
	 * 
	 * @return the person id
	 */
	public String getPersonId() {

		return personId;
	}

	/**
	 * Gets the user privileges.
	 * 
	 * @return the user privileges
	 */
	public Collection<UserPriv> getUserPrivileges() {

		return userPrivileges;
	}

	/**
	 * Checks if is access granted.
	 * 
	 * @return true, if is access granted
	 */
	public boolean isAccessGranted() {

		return accessGranted;
	}

	public String getNtLogin() {
		return ntLogin;
	}

	public String getPersonTitle() {
		return personTitle;
	}

	public Long getGroups() {
		return groups;
	}

}