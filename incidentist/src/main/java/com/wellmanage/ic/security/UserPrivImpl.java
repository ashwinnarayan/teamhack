package com.wellmanage.ic.security;

import com.wellmanage.ic.dto.BaseDto;

/**
 * The Class UserPrivImpl.
 */
public class UserPrivImpl extends BaseDto implements UserPriv, Comparable<UserPrivImpl> {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The authority. */
    private final String authority;

    /**
     * Instantiates a new user priv impl.
     *
     * @param authority the authority
     */
    public UserPrivImpl(String authority) {

        super();
        this.authority = authority;
    }

    /**
     * {@inheritDoc}
     *
     * @see org.springframework.security.core.GrantedAuthority#getAuthority()
     */
    @Override
    public String getAuthority() {
        return authority;
    }    
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((authority == null) ? 0 : authority.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserPrivImpl other = (UserPrivImpl) obj;
		if (authority == null) {
			if (other.authority != null)
				return false;
		} else if (!authority.equals(other.authority))
			return false;
		return true;
	}

	@Override
	public int compareTo(UserPrivImpl o) {
		return authority.compareTo(o.getAuthority());
	}
}