package com.wellmanage.ic.security;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

/**
 * The Class HantwebAuthenticationManager.
 */
@Service
@ComponentScan
public class HantwebAuthenticationManager implements AuthenticationManager {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(HantwebAuthenticationManager.class);

	/** The wal authentication. */
	@Autowired
	private WALAuthentication walAuthentication;

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.security.authentication.AuthenticationManager#authenticate(org.springframework.security.core.Authentication)
	 */
	@Override
	public Authentication authenticate(Authentication authentication) {

		// TODO Auto-generated method stub
		HantwebPrincipal principal = (HantwebPrincipal) authentication
				.getPrincipal();
		LOGGER.info("Authenticate: " + principal);
		Collection<UserPriv> userPrivs = walAuthentication
				.getWALEntitlements(principal.getUserLogin());
		Authentication token = new RememberMeAuthenticationToken(
				principal.getUserLogin(), authentication.getPrincipal(),
				userPrivs);
		return token;

	}
}