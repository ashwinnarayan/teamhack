package com.wellmanage.ic.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wellmanage.ic.domain.Chat;

public interface ChatRepository extends JpaRepository<Chat, Integer>{

}