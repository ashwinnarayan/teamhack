(function(angular) {
	angular.module("chatApp.controllers").controller(
			"ChatCtrl",
			function($scope, $http, $timeout, ChatService, Incident) {

				$scope.messages = [];
				$scope.newIncidentEntry = false;
				$scope.activeIncident = undefined;

				var getUser = function() {
					$http.get('api/loggedInUser').success(function(data) {
						$scope.username = data;
					});
				};

				getUser();

				var init = function() {
					$scope.newIncidentEntry = false;
					$scope.message = "";
					$scope.newSeverity = "";
					$scope.newApp = "";
					$scope.newMgr = "";
					$scope.newInterval = "";
					$scope.newTeam = "";
				};

				init();

				$scope.newIncidentEntryWorkflow = function() {
					init();
					$scope.newIncidentEntry = true;
				};

				$scope.isEmptyObject = function(item) {
					return angular.equals({}, item);
				};

				$scope.addMessage = function() {
					var messageItem = {};
					messageItem.message = $scope.message;
					messageItem.user = $scope.username.userLogin;
					messageItem.timestamp = new Date();
					ChatService.send(messageItem);
					$scope.message = "";
				};

				ChatService.receive().then(null, null, function(message) {
					$scope.messages.push(message);
				});

				$scope.reset = function() {
					init();
				};

				$scope.addIncidentItem = function(newTeam, newMgr, newSeverity,
						newSeverityLevel, newIncident, newIncFlag, newApp) {
					if (newSeverity == 'other' && newSeverityLevel) {
						newSeverity == 'sev' + newSeverityLevel;
					}
					new Incident({
						incidentManager : newMgr,
						applicationName : newApp,
						assignmentGroup : newTeam,
						severity : newSeverity,
						user : $scope.username.userLogin,
						timestamp : new Date(),
						status : "Active"
					}).$save(function(item) {
						if (!newIncFlag) {
							item.id = newIncident;
						}
						$scope.activeIncident = item;
					});
					init();
				};

			});
})(angular);