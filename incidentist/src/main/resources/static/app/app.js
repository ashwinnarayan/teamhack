(function(angular) {
	angular.module("chatApp", [ "chatApp.controllers", "chatApp.services" ]);
	angular.module("chatApp.services", ['ngResource']);
	angular.module("chatApp.controllers", []).$inject = [ '$scope', '$http',
			'$timeout', 'ChatService', 'Incident' ];
	;
})(angular);