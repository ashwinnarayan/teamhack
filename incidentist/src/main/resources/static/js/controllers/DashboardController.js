angular.module('MetronicApp').controller(
		'DashboardController',
		function($rootScope, $scope, $http, $timeout, $state, $stateParams, ChatService) {

			var refreshTimerInterval = 1;
			$scope.chatMsgs = [];

			$scope.$on('$viewContentLoaded', function() {
				// initialize core components
				App.initAjax();
			});

			$scope.getUser = function() {
				$http.get("api/loggedInUser").success(function(data) {
					$rootScope.username = data;
				});
			};

			ChatService.receive().then(null, null, function(message) {
				$scope.chatMsgs.push(message);
			});

			$scope.getChats = function() {
				$http.get("api/chats/byIncident/"+$rootScope.incidentId).success(function(data) {
					$scope.chatMsgs = data;
				});
			};

			$scope.addChat = function(item) {
				$http.post("api/chats", item).success(function(data) {
				});
			};

			$scope.addMessage = function() {
				var messageItem = {};
				messageItem.message = $scope.chatMessage;
				$scope.chatMessage = "";
				messageItem.user = $rootScope.username.username;
				messageItem.timestamp = new Date();	
				messageItem.incidentId = $rootScope.incidentId;
				ChatService.send(messageItem);
				$scope.addChat(messageItem);
			};
			
			$scope.tagChat = function(message) {
				$http.post('/api/chats/tagChat', message).success(function(data) {					
				});
			}

			$scope.intervalFunction = function() {
				$scope.counter = refreshTimerInterval;
				$scope.onTimeout = function() {
					$scope.counter--;
					if ($scope.counter < 1) {
						$scope.counter = refreshTimerInterval;
						$scope.getChats();
					}
					mytimeout = $timeout($scope.onTimeout, 1000);
				}
				var mytimeout = $timeout($scope.onTimeout, 1000);
			};

			$scope.getUser();

			$scope.intervalFunction();

			if ($rootScope.incidentId != null
					&& $stateParams.incidentId == null) {
				$scope.incidentId = $rootScope.incidentId;
			} else {
				$scope.incidentId = $stateParams.incidentId;
				$rootScope.incidentId = $stateParams.incidentId;
			}
			// set sidebar closed and body solid layout mode
			$rootScope.settings.layout.pageContentWhite = true;
			$rootScope.settings.layout.pageBodySolid = false;
			$rootScope.settings.layout.pageSidebarClosed = false;
			$rootScope.itltStartTime = new Date().getTime() - 600000;
			$rootScope.bizStartTime = new Date().getTime() - 500000;
			$rootScope.incidentOpenTime = new Date().getTime() - 1200000;
		});
angular.module('DashboardController', []).$inject = [ '$scope', '$http',
		'$timeout', 'ChatService' ];
angular
		.module('MetronicApp')
		.controller(
				'itltCommunicationCtrl',
				function($scope) {
					$scope.tinymceOptions = {
						onChange : function(e) {
							// put logic here for keypress and cut/paste changes
						},
						inline : false,
						plugins : 'advlist autolink link image lists charmap print preview',
						skin : 'lightgray',
						theme : 'modern',
						menubar : 'file edit'
					};
				});
angular.module("MetronicApp").controller(
		"tasksCtrl",
		function($rootScope, $scope, $http, $timeout) {
			var getTasks = function() {
				$http.get(
						'api/serviceNow/tasks?incidentId='
								+ $rootScope.incidentId).success(
						function(data) {
							$scope.tasks = data;
						})
			};
			var refreshTimerInterval = 5;

			$scope.taskTimer = function(refreshTimerInterval) {
				$scope.counter = refreshTimerInterval;
				$scope.onTimeout = function() {
					$scope.counter--;
					if ($scope.counter < 1) {
						$scope.counter = refreshTimerInterval;
						getTasks();
					}
					mytimeout = $timeout($scope.onTimeout, 1000);
				}
				var mytimeout = $timeout($scope.onTimeout, 1000);
			};
			getTasks();
			$scope.taskTimer(refreshTimerInterval);
		});

// itltCommCtrl

angular.module("MetronicApp").controller(
		"itltCommCtrl",
		function($rootScope, $scope, $http, $timeout) {
			var getITLTComm = function() {
				$http.get(
						'api/incidentMgmt/itltComm?incidentId='
								+ $rootScope.incidentId).success(
						function(data) {
							$scope.tasks = data;
						})
			};
			var refreshTimerInterval = 20;

			$scope.taskTimer = function(refreshTimerInterval) {
				$scope.counter = refreshTimerInterval;
				$scope.onTimeout = function() {
					$scope.counter--;
					if ($scope.counter < 1) {
						$scope.counter = refreshTimerInterval;
						// getITLTComm();
					}
					mytimeout = $timeout($scope.onTimeout, 1000);
				}
				var mytimeout = $timeout($scope.onTimeout, 1000);
			};
			// getITLTComm();
			$scope.taskTimer(refreshTimerInterval);
		});

angular
		.module("MetronicApp")
		.controller(
				"bizCommCtrl",
				function($rootScope, $scope, $http, $timeout) {
					var getBizComm = function() {
						$http.get(
								'api/incidentMgmt/bizComm?incidentId='
										+ $rootScope.incidentId).success(
								function(data) {
									$scope.tasks = data;
								})
					};
					var refreshTimerInterval = 20;

					$scope.taskTimer = function(refreshTimerInterval) {
						$scope.counter = refreshTimerInterval;
						$scope.onTimeout = function() {
							$scope.counter--;
							if ($scope.counter < 1) {
								$scope.counter = refreshTimerInterval;
								// getBizComm();
							}
							mytimeout = $timeout($scope.onTimeout, 1000);
						}
						var mytimeout = $timeout($scope.onTimeout, 1000);
					};
					// getBizComm();
					$scope.taskTimer(refreshTimerInterval);
					$scope.tinymceOptions = {
						inline : false,
						plugins : "emoticons template paste textcolor colorpicker textpattern imagetools",
						skin : 'lightgray',
						theme : 'modern',
						menubar : false,
						body_class : "form-control",
						elementpath : false,
						content_css : "css/tinymcecontent.css?"
								+ new Date().getTime(),
						element_format : "html",
						font_formats : "Andale Mono=andale mono,times;"
								+ "Arial=arial,helvetica,sans-serif;"
								+ "Arial Black=arial black,avant garde;"
								+ "Book Antiqua=book antiqua,palatino;"
								+ "Comic Sans MS=comic sans ms,sans-serif;"
								+ "Courier New=courier new,courier;"
								+ "Georgia=georgia,palatino;"
								+ "Helvetica=helvetica;"
								+ "Impact=impact,chicago;" + "Symbol=symbol;"
								+ "Tahoma=tahoma,arial,helvetica,sans-serif;"
								+ "Terminal=terminal,monaco;"
								+ "Times New Roman=times new roman,times;"
								+ "Trebuchet MS=trebuchet ms,geneva;"
								+ "Verdana=verdana,geneva;"
								+ "Webdings=webdings;"
								+ "Wingdings=wingdings,zapf dingbats",
						fontsize_formats : "8pt 10pt 11pt 11pt 12pt 14pt 18pt 24pt 36pt",
						resize : false,
						browser_spellcheck : true,
						toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontselect | fontsizeselect | forecolor backcolor"
					};

				});
