/**
 * 
 */
// landing
angular.module('MetronicApp').controller(
		"landingCtrl",
		function($state, $rootScope, $scope, $http) {

			$scope.getUser = function() {
				function success(result) {
					//alert(JSON.stringify(result));
					$rootScope.username = result.data;
					$http.get("api/snUser?userId=" + result.data.username)
					.success(function(data) {
						$rootScope.user = data;
					});
				}
				function error(err) {
					return err.data;
				}
				return $http({
					url : 'api/loggedInUser',
					method : "GET",
					timeout : 200000
				}).then(success, error);
			};

/*			$scope.getIncidents = function() {
				$http.get('api/')
			};
			*/
			$scope.getUser();
			
			

			var form = $('.landing-form');

			var formObj = $('#landingForm');
			var input = $('input', form);
			var btn = $('.btn', form);

			var handleClick = function(e) {
				e.preventDefault();

				var text = input.val();
				if (text.length == 0) {
					return;
				}

				$state.go("dashboardInc", {
					incidentId : text
				});
			}

			$('body').on('click', '.message .name', function(e) {
				e.preventDefault(); // prevent click event

				var name = $(this).text(); // get clicked user's full name
				input.val('@' + name + ':'); // set it into the input field
				App.scrollTo(input); // scroll to input if needed
			});

			btn.click(handleClick);

			input.keypress(function(e) {
				if (e.which == 13) {
					handleClick(e);
					return false; // <---- Add this line
				}
			});
		});
// end landing
angular.module("MetronicApp").controller('activeIncCtrl', function($scope, $http) {
	
	$scope.activeIncidents = [];
	
	$scope.findTop10 = function(userId) {
		$http.get('/api/serviceNow/criticalIncidents').success(function(data) {
			$scope.activeIncidents = data.result;
		});
	};
	
	$scope.findTop10();
	
});